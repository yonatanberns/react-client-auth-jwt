import React, { useState, useEffect, useRef } from 'react';
import logo from './logo.svg';
import './App.css';
import * as api from './api';
import {FaFacebookF,FaGithub} from 'react-icons/fa'
import qs from 'query-string'
import styled from 'styled-components'

function App() {
  const [authenticated,set_authenticated] = useState(false);
  const [loading,set_loading] = useState(false);
  const [user,set_user] = useState(null);
  const [access_token,set_access_token] = useState(null);
  const [content,set_content] = useState('no content yet...');

  const firstNameRef = useRef(null);
  const lastNameRef = useRef(null);
  const emailRef = useRef(null);
  const phoneRef = useRef(null);
  const passwordRef = useRef(null);
  const logInEmailRef = useRef(null);
  const logInPswrdRef = useRef(null);

  const [display_register, set_display_register] = useState(true);
  const [display_logout, set_display_logout] = useState(false);

  useEffect(()=>{
    if(window.location.search && window.location.search.includes('token')){
      console.log('search - ',window.location.search)
      const search = qs.parse(window.location.search);
      console.log('qs - ',search)
      const profile = JSON.parse(search.profile)
      const {token} = search
      console.log('profile - ',profile)
      console.log('token - ',token)
      if(profile && token){
       set_authenticated(true)
       set_user(profile)
       //  localStorage.setItem('token',token) //not secure
       set_access_token(token) //store access_token in-memory
 
       //remove query string and path from url...
       window.history.replaceState({}, document.title, "/");
      }
    }else if(!access_token){
      //ask access_token from server, in case refresh_token already exists as an httpOnly cookie
      const get_access = async ()=> {
          const response = await api.get_access_token();
          if('access_token' in response && 'profile' in response){
            set_access_token(response.access_token);
            set_authenticated(true);
            set_user(JSON.parse(response.profile))
          }else{
            console.log('please login');
          }
      }
      get_access()
    }
  },[access_token])

  const connect = provider =>{
     set_loading(true)
     window.location.href = `http://localhost:3030/api/auth/${provider}`;  
  }

  const logout = async _ =>{
    const response = await api.logout()
    if(response.status === 'You are logged out'){
      set_authenticated(false)
      set_user(null)
      // localStorage.removeItem('token') //not secure
      set_access_token(null)
      set_display_logout(false);
    }
  }
  const get_protected = async _ =>{
    //const token = localStorage.getItem('token') //not secure
    const token = access_token;
    const response = await api.get_protected({token})
    set_content(response.payload)
  }
  
  const handleRegisterSubmit = (e)=> {
      e.preventDefault();
      console.log('click submit register!')
      console.log(firstNameRef.current.value, lastNameRef.current.value, emailRef.current.value, phoneRef.current.value, passwordRef.current.value);
      set_display_register(false);
      api.register({first_name: firstNameRef.current.value, 
      last_name: lastNameRef.current.value, 
      email: emailRef.current.value,
      phone: phoneRef.current.value,
      password: passwordRef.current.value 
    });
  }

  const handleLoginSubmit = async (e)=> {
      e.preventDefault();
      console.log('click Log-in!')
      console.log(logInEmailRef.current.value, logInPswrdRef.current.value);
    
      try {
        const resp_token = await api.login({
        email: logInEmailRef.current.value,
        password: logInPswrdRef.current.value 
      });
      console.log('new token',resp_token);
        set_access_token(resp_token);
        if(resp_token) {
          set_display_logout(true);
        } else {
          alert('Invalid email adress or password.. please try again');
        }
    } catch(er) {
      console.log('error in client login', er);
    }

  }
  
  return (
    <div className="App">
     
        <div className="top-box">
          <h1>Auth demo</h1>
          <img src={logo} className="logo" alt="logo" />
        </div>
        
        <p>Authenticated:<span className="lightblue-text">{' '+authenticated}</span></p>
        <p>Loading:<span className="lightblue-text">{' '+loading}</span></p>
        <div className="profile-box">
          {user && <>
            <p>{user.name}</p>
            <img className="profile-img" src={user && user.photo} alt="avatar" />
            </>}
        </div>
        <div className="buttons-box">
          {authenticated?
          <button className="btn" onClick={logout}>Logout</button>
          : <div>
              <button className="oauth-btn" onClick={()=> connect('github')}><FaGithub/></button>
              <button className="oauth-btn" onClick={()=> connect('facebook')}><FaFacebookF/></button>
            </div>
          }
          <button className="btn" onClick={get_protected}>get protected content</button>
        </div>
        <p className="content">{content}</p>

        
      {display_register?
      <>
        <FormDiv>
          <h3>register enter details:</h3>
          <input ref={firstNameRef} placeholder="first name" />
          <input ref={lastNameRef} placeholder="last name"/>
          <input ref={emailRef} placeholder="email"/>
          <input ref={phoneRef} placeholder="phone"/>
          <input ref={passwordRef} placeholder="password"/>
          <Button type="submit" onClick={handleRegisterSubmit}>submit register</Button>
        </FormDiv>
      </>
      : 
      <>
      {!display_logout ?
        <FormDiv>
          <h2>log in form</h2>
          <input ref={logInEmailRef} placeholder="email"/>
          <input ref={logInPswrdRef} placeholder="password"/>
          <Button type="submit" onClick={handleLoginSubmit}>submit log-in</Button>
        </FormDiv>
      : <>
        <button className="btn" onClick={logout}>Logout</button>
       </> }
      </>} 
    </div>
  );
}

export default App;



const FormDiv = styled.form`

  // background-color: blue;
  margin-top: 2rem;
  border: 2px solid white;
  padding: 1rem;
  // position: absolute;
  // display: flex:
  // flex-direction: column;
  // flex-wrap: nowrap;
  display: grid;
  gap: 1rem;
`

const Button = styled.button`
  cursor: pointer;

`


//:<button className="btn" onClick={login}>Login</button>}