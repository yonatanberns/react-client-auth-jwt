//http://localhost:3030
import axios from 'axios'

export const get_access_token = async ()=> {
    const url = '/api/auth/get-access-token';
    const response = await (await fetch(url)).json()
    console.log({response})
    return response;
}
export const logout = async ()=> {
    const url = '/api/auth/logout';
    const response = await (await fetch(url)).json()
    console.log({response})
    return response;
}

export const login = async (user_data)=> {
    console.log('in log-in post',user_data);
    try {
        const resp = await axios.post('http://localhost:3030/api/auth/login', user_data);
        console.log('recived post-->',resp.data);
        return resp.data.token;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const register = async (user_data)=> {
    console.log('in send post',user_data);
    try {
        const resp = await axios.post('http://localhost:3030/api/auth/register', user_data);
        console.log('recived post-->',resp.data);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const get_protected = async ({token})=> {
    const url = 'api/auth/protected';
    const headers = {'x-access-token': token}

    const response = await (await fetch(url,{headers})).json()
    console.log({response})
    return response;
}